﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SessionMVC1212387.Models
{
    interface IUserRepositoryModel
    {
        IEnumerable<UsersModel> getAllUser1212387();
        UsersModel getUserByID1212387(string mssv);
    }
}
