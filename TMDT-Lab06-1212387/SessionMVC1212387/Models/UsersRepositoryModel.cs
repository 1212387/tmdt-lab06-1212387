﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SessionMVC1212387.Models
{
    public class UsersRepositoryModel : IUserRepositoryModel
    {
        private List<UsersModel> listuser = new List<UsersModel>();
        public UsersRepositoryModel()
        {
            listuser.Add(new UsersModel { name = "Trương Mai Minh Trí", mssv = "1212446", email = "1212446@student.hcmus.edu.vn" });
            listuser.Add(new UsersModel { name = "Nguyễn Hữu Vinh", mssv = "1212511", email = "1212511@student.hcmus.edu.vn" });
            listuser.Add(new UsersModel { name = "Nguyễn Trần Phước Thọ	", mssv = "1212387", email = "1212387@student.hcmus.edu.vn" });
            listuser.Add(new UsersModel { name = "Hồ Thế Tông", mssv = "1212427", email = "1212427@student.hcmus.edu.vn" });
            listuser.Add(new UsersModel { name = "Phan Hiền Triết", mssv = "1212436", email = "1212436@student.hcmus.edu.vn" });
            listuser.Add(new UsersModel { name = "Đoàn Xuân Trí", mssv = "1212439", email = "1212439@student.hcmus.edu.vn" });
        }



        public IEnumerable<UsersModel> getAllUser1212387()
        {
            return listuser;
        }

        public UsersModel getUserByID1212387(string mssv)
        {
            return listuser.Find(user => user.mssv == mssv);
        }
    }
}