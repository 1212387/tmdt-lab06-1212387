﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SessionMVC1212387.Models;

namespace SessionMVC1212387.Controllers
{
    public class usersController : Controller
    {
        //
        // GET: /users/

        static readonly IUserRepositoryModel Users = new UsersRepositoryModel();
        public ActionResult Index()
        {

            return View("Index");
        }

        [Route("users")]
        public ActionResult loadUser1212387()
        {
            System.Web.HttpContext.Current.Session["lastURL"] = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
            try
            {
                if ((bool)System.Web.HttpContext.Current.Session["sessionLogin"] == true)
                {
                    
                    return View("listUser", Users.getAllUser1212387());
                }
                else
                {
                    return RedirectToAction("login1212387", "auth");
                }
            }
            catch { return RedirectToAction("login1212387", "auth"); }
        }

       
        public ActionResult loadUserByMSSV1212387(string mssv)
        {
            System.Web.HttpContext.Current.Session["lastURL"] = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
            try
            {
                if ((bool)System.Web.HttpContext.Current.Session["sessionLogin"] == true)
                {

                    return View("User", Users.getUserByID1212387(mssv));
                }
                else
                {
                    return RedirectToAction("login1212387", "auth");
                   
                   
                }
            }
            catch { return RedirectToAction("login1212387", "auth"); }
            
        }
	}
}